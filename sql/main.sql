SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `cliente` (
  `codice_fiscale` varchar(16) NOT NULL,
  `nome` varchar(64) NOT NULL,
  `cognome` varchar(64) NOT NULL,
  `fatturazione` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `hash_password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `dipendente` (
  `codice_fiscale` varchar(16) NOT NULL,
  `nome` int NOT NULL,
  `cognome` int NOT NULL,
  `stipendio_medio` int NOT NULL,
  `codice_dipendente` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entry` (
  `id` int NOT NULL,
  `ordine` int NOT NULL,
  `prodotto` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ordine` (
  `id` int NOT NULL,
  `punto_vendita` varchar(64) NOT NULL,
  `client` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `prodotto` (
  `codice_barre` int NOT NULL,
  `nome` varchar(64) NOT NULL,
  `descrizione` varchar(256) DEFAULT NULL,
  `prezzo` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `punto_vendita` (
  `nome` varchar(64) NOT NULL,
  `descrizione` varchar(256) NOT NULL,
  `indirizzo` varchar(256) NOT NULL,
  `online` tinyint(1) NOT NULL,
  `manager` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quantita` (
  `id` int NOT NULL,
  `prodotto` int NOT NULL,
  `punto_vendita` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `quantita` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `reparto` (
  `id` int NOT NULL,
  `nome` varchar(64) NOT NULL,
  `descrizione` varchar(256) NOT NULL,
  `punto_vendita` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


ALTER TABLE `cliente`
  ADD PRIMARY KEY (`codice_fiscale`),
  ADD UNIQUE KEY `username` (`username`);

ALTER TABLE `dipendente`
  ADD PRIMARY KEY (`codice_fiscale`),
  ADD UNIQUE KEY `codice_dipendente` (`codice_dipendente`);

ALTER TABLE `entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ordine` (`ordine`),
  ADD KEY `prodotto` (`prodotto`);

ALTER TABLE `ordine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client` (`client`),
  ADD KEY `punto_vendita` (`punto_vendita`);

ALTER TABLE `prodotto`
  ADD UNIQUE KEY `codice_barre` (`codice_barre`);

ALTER TABLE `punto_vendita`
  ADD PRIMARY KEY (`nome`),
  ADD KEY `manager` (`manager`);

ALTER TABLE `quantita`
  ADD KEY `prodotto` (`prodotto`),
  ADD KEY `punto_vendita` (`punto_vendita`);

ALTER TABLE `reparto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `punto_vendita` (`punto_vendita`);


ALTER TABLE `entry`
  ADD CONSTRAINT `entry_ibfk_1` FOREIGN KEY (`ordine`) REFERENCES `ordine` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `entry_ibfk_2` FOREIGN KEY (`prodotto`) REFERENCES `prodotto` (`codice_barre`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `ordine`
  ADD CONSTRAINT `ordine_ibfk_1` FOREIGN KEY (`client`) REFERENCES `cliente` (`codice_fiscale`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `ordine_ibfk_2` FOREIGN KEY (`punto_vendita`) REFERENCES `punto_vendita` (`nome`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `punto_vendita`
  ADD CONSTRAINT `punto_vendita_ibfk_1` FOREIGN KEY (`manager`) REFERENCES `dipendente` (`codice_fiscale`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `quantita`
  ADD CONSTRAINT `quantita_ibfk_1` FOREIGN KEY (`prodotto`) REFERENCES `prodotto` (`codice_barre`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `quantita_ibfk_2` FOREIGN KEY (`punto_vendita`) REFERENCES `punto_vendita` (`nome`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `reparto`
  ADD CONSTRAINT `reparto_ibfk_1` FOREIGN KEY (`punto_vendita`) REFERENCES `punto_vendita` (`nome`) ON DELETE RESTRICT ON UPDATE RESTRICT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
